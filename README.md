[![Codacy Badge](https://api.codacy.com/project/badge/Grade/68b165bf42954f8cbff368e00464c319)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=qamine/qa-automation&amp;utm_campaign=Badge_Grade_Dashboard)

**How to Set the Enviorment and Run the Automatic Tests**


## Clone the Repository and Configure

1. clone the "https://bitbucket.org/qamine/codacy-website-qa-automation/src/master/"
2. Install maven "brew install maven"
3. Install JDK 8 
4. In the intellij give the path to the JDK
5. File - Project Settings - JDK - JDK HOME
6. Install Selenium StandAlone -  brew install selenium-server-standalone
7. Install ChromeDriver - brew install chromedriver
8. Run $ docker run -d -p 4444:4444 -v /dev/shm:/dev/shm selenium/standalone-chrome
9. 	go to localhost:4444 , a Se Standalone page should open
10. you�re ready to run the tests :)


## Run the tests
1. Go to the folder of the cloned Project
2. terminal here
3. mvn clean test, this will run the pre-configured testsuite
4. to run a specific suite, mvn test -Dsurefire.suiteXmlFiles=Suite/Web/XXX.xml
5. browsers will open and tests will start
6. the report will be published at .ExtentReports

## Configure the Suites 
1. Open the the project with IntellIj or other program of your choosing
2. Go to the Suite Folder
3. Suites are devided by Web, Api and Cli
3. Open ex: Prod.xml file
4. thread-count you configure how many threads in Paralell
5. you can add or remove tests from the suite  

### Block Code

Suite example

    <suite name="BDD Test Suite" verbose="1" parallel="tests" thread-count="3" configfailurepolicy="continue" >
	    <listeners>
	        <listener class-name="org.codacy.TestListenerAPI"/>
	        <listener class-name="org.codacy.AnnotationTransformerAPI"/>
	    </listeners>
    <parameter name="environment" value="QA"></parameter>
    <parameter name="browser" value="chrome"></parameter>
    	<test name="Validate HomePage" annotations="JDK" preserve-order="true">
	        <classes>
	            <class name="api.TC_11_API_PostProjectUpdate"></class>
	        </classes>
   		</test>
    </suite>
    
    
    
## See the Browser inside the Docker

 1. Donwload vnc viewer - https://www.realvnc.com/download/viewer/
 2. in the terminal run the follow command: docker run -d -p 4444:4444 -p 5900:5900 -v /dev/shm:/dev/shm selenium/standalone-chrome-debug
 3. Open vnc viewer and add localhost:5900 , password is "secret"
 4. Open the Windown you just created in vnc viewer
 4. Run the test :)

 

    
    

